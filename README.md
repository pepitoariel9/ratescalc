## How to run the test

-   Install all the dependencies using composer

```
    composer install
```

-   Exercise 1

```
    php artisan exercise:1
```

-   Exercise 2

```
    php artisan exercise:2
```

-   Exercise 3

```
    php artisan exercise:3
```

-   Exercise 4

```
    php artisan exercise:4
```

-   Exercise 5

```
    php artisan exercise:5
```
