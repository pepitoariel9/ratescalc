<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Exercise2Command extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exercise:2';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ratescalc Pre-Interview Coding Test - exercise 2';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $input = $this->ask('Enter two array lists');

        $this->line(combined_two_lists($input));
    }
}
