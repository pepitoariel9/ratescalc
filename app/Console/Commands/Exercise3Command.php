<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Exercise3Command extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exercise:3';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ratescalc Pre-Interview Coding Test - exercise 3';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $input = $this->ask('Enter array of integers (separated by comma)');

        $this->line(calculate_array_points($input));
    }
}
