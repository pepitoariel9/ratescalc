<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Exercise5Command extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exercise:5';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ratescalc Pre-Interview Coding Test - exercise 5';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $studentName = $this->ask('Enter list of strings in an array form');

        print generate_frame_from_arrays(json_decode($studentName));
    }
}
