<?php

namespace App\Console\Commands;

use App\Exercises\Exercise4\Student;
use Illuminate\Console\Command;

class Exercise4Command extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exercise:4';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ratescalc Pre-Interview Coding Test - exercise 4';

    protected $student;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->createStudent();
    }

    public function createStudent()
    {
        $studentName = $this->ask('Enter student name');

        $grade = $this->choice('Select student grade', ['Grade 1', 'Grade 2','Grade 3','Grade 4','Grade 5','Grade 6']);

        if (!$this->student) {
            $this->student = (new Student($studentName, $grade))->save();
        } else {
            $this->student->create($studentName, $grade)->save();
        }

        $this->line('Current Lists:');

        $lists = $this->student->all();
        foreach ($lists as $grade => $students) {
            $this->line($grade);

            foreach ($students as $student) {
                $this->line("   {$student}");
            }
        }
        
        if ($this->confirm('Do you want to create more student?')) {
            $this->createStudent();
        }
    }
}
