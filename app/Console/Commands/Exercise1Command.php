<?php

namespace App\Console\Commands;

use App\Exercises\Exercise1\Translator;
use Illuminate\Console\Command;

class Exercise1Command extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exercise:1';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ratescalc Pre-Interview Coding Test - exercise 1';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $input1 = $this->ask('English words to Pig Latin words');

        $this->line(Translator::translate($input1)->toPigLatin());

        $input2 = $this->ask('Pig Latin words to English');

        $this->line(Translator::translate($input2)->toEnglish());
    }
}
