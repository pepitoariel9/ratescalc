<?php

namespace App\Console;

use App\Console\Commands\Exercise1Command;
use App\Console\Commands\Exercise2Command;
use App\Console\Commands\Exercise3Command;
use App\Console\Commands\Exercise4Command;
use App\Console\Commands\Exercise5Command;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Exercise1Command::class,
        Exercise2Command::class,
        Exercise3Command::class,
        Exercise4Command::class,
        Exercise5Command::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
