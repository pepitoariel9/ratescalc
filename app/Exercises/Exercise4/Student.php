<?php

namespace App\Exercises\Exercise4;

class Student
{
    public $name;

    public $grade;

    protected $studentLists = [];

    public function __construct(string $name, string $grade)
    {
        $this->name    = $name;
        $this->grade   = $grade;
    }

    protected function store()
    {
        $this->studentLists[$this->grade][] = $this->name;
        return $this;
    }

    public function save()
    {
        $this->studentLists[$this->grade][] = $this->name;

        return $this;
    }
    public function create(string $name, string $grade)
    {
        $this->name    = $name;
        $this->grade   = $grade;

        return $this;
    }


    public function all()
    {
        $students = collect($this->studentLists);

        return $students->sortKeys()->map(function ($list) {
            return collect($list)->sort()->all();
        })->all();
    }
}
