<?php

if (!function_exists('combined_array_to_string')) {
    function combined_array_to_string(array $list1, array $list2)
    {
        $newArray = [];

        for ($l1 = 0; $l1 < count($list1); $l1++) {
            $newArray[] = $list1[$l1];
            for ($l2 = 0; $l2 < count($list2); $l2++) {
                if ($l1 === $l2) {
                    $newArray[] = $list2[$l1];
                }
            }
        }

        return '['.implode(',', $newArray).']';
    }
}


if (!function_exists('combined_two_lists')) {
    function combined_two_lists(string $string)
    {
        $chars = explode(',', $string);

        $lists      = [];
        $totalLists = 0;

        foreach ($chars as $index => $char) {
            $char      = trim($char);
            $charCount = strlen($char);

            if (strrpos($char, "[") !== false) {
                $totalLists++;
                $char = substr($char, 1);
            }

            if (strrpos($char, "]") !== false) {
                $char = substr($char, 0, ($charCount - 1));
            }


            $lists[$totalLists][] = $char;
        }

        return combined_array_to_string($lists[1], $lists[2]);
    }
}
