<?php

namespace App\Exercises\Exercise1;

class Translator
{
    const APPENDED_TEXT = 'ay';

    protected $words;

    public static function translate(string $words)
    {
        return new Self($words);
    }

    public function __construct(string $words)
    {
        $this->words = $words;
    }

    public function toPigLatin()
    {
        $arrayWords = explode(' ', $this->words);

        $newWords = [];

        foreach ($arrayWords as $word) {
            $newWords[] = strtolower(substr($word, 1) . substr($word, 0, 1) . self::APPENDED_TEXT);
        }

        return ucfirst(implode(' ', $newWords));
    }

    public function toEnglish()
    {
        $arrayWords = explode(' ', $this->words);

        $newWords = [];

        foreach ($arrayWords as $word) {
            $countChars = strlen($word);
            $actualChar = ($countChars - 3);

            $firstChar = substr($word, $actualChar, 1);
            $newWord   = substr($word, 0, $actualChar);

            $newWords[] = strtolower($firstChar . $newWord);
        }

        return ucfirst(implode(' ', $newWords));
    }
}
