<?php

if (!function_exists('calculate_array_points')) {
    function calculate_array_points(string $input)
    {
        $inputArray = explode(',', $input);

        $points = 0;

        foreach ($inputArray as $value) {
            if (!is_numeric($value)) {
                throw new \Exception('Array value must be a number');
                break;
            }

            if ($value == 8) {
                $points += 5;
                continue;
            }

            if ($value%2 === 0) {
                $points += 1;
            } else {
                $points += 3;
            }
        }

        return $points;
    }
}
