<?php

if (!function_exists('generate_frame_from_arrays')) {
    function generate_frame_from_arrays(array $strings)
    {
        $higherLength = 0;

        foreach ($strings as $string) {
            $countChars = strlen($string);

            if ($countChars > $higherLength) {
                $higherLength = $countChars;
            }
        }

        $newHigherLength = $higherLength + 4;

        print_asterisk($newHigherLength);

        print(PHP_EOL);

        foreach ($strings as $index => $string) {
            $space = '';

            for ($i = 0; $i < ($higherLength - strlen($string)); $i++) {
                $space .= ' ';
            }

            print('* '.$string.$space.' *');

            if (($index + 1) < count($strings)) {
                print(PHP_EOL);
            }
        }

        print(PHP_EOL);

        print_asterisk($newHigherLength);
    }
}

if (!function_exists('print_asterisk')) {
    function print_asterisk(int $newHigherLength)
    {
        for ($i = 0; $i < $newHigherLength ; $i++) {
            print('*');
        }
    }
}
